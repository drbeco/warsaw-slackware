# Creating a slackware pkg
# 2022-11-12 by drbeco

# From this repository
#git clone https://gitlab.com/drbeco/warsaw-slackware.git
#cd warsaw-slackware

TODAY=$(date +"%Y%m%d")

. warsaw-slackware.info
# echo Before updating: $PRGNAM-$VERSION-$ARCH-$BUILD

if grep -q "VERSION=\"$TODAY\"" warsaw-slackware.info ; then
    # add to build
    echo "Version is already updated, changing build"
    (( BUILD++ ))
    sed -i "s/BUILD=\".*\"/BUILD=\"$BUILD\"/" warsaw-slackware.info
else
    # change version, build=1
    echo "Version is now $TODAY"
    sed -i 's/BUILD=".*"/BUILD="1"/' warsaw-slackware.info
    sed -i "s/VERSION=\".*\"/VERSION=\"$TODAY\"/" warsaw-slackware.info
fi

echo
cat warsaw-slackware.info 
echo
. warsaw-slackware.info
cd pkg || exit 1
makepkg -l y -c n /tmp/$PRGNAM-$VERSION-$ARCH-$BUILD.txz


