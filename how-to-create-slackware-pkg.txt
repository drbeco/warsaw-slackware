# How to create a slackware pkg
# 2022-10-10 by Dr. Beco
# 2022-11-12


# From this repository

git clone https://gitlab.com/drbeco/warsaw-slackware.git
cd warsaw-slackware
. warsaw-slackware.info
makepkg -l y -c n /tmp/$PRGNAM-$VERSION-$ARCH-$BUILD.txz



# from .deb

mkdir -p pkgname-work/pkgname-pkg/pkg
cd pkgname-work
wget pkgname.deb

ar -x pkgname.deb
# this creates pkgname-work/data.tar.xz and pkgname-work/control.tar.xz

cd pkgname-pkg/pkg
tar xvf ../../data.tar.xz

Then, as root, you run makepkg in the work/pkg directory, like

cd ..
makepkg -l y -c n /tmp/pkgname-version-arch-build.txz




