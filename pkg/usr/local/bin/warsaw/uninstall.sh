#!/bin/bash
# by Ruben Carlo Benante
# Slackware 15 , 2022-10-09
# Uninstall warsaw

if [ "$(whoami)" != "root" ]; then
  exit 2
fi

rm -f /etc/rc.d/init.d/warsaw
rm -f /etc/warsaw.conf
rm -f /lib/systemd/system/warsaw.service
rm -f /usr/bin/warsaw

rm -rf /usr/local/bin/warsaw
rm -rf /usr/local/lib/warsaw
rm -rf /usr/local/etc/warsaw
rm -rf /usr/doc/warsaw

rm -f /usr/local/share/fonts/TTF/dbldwrsw.ttf
rm -f /usr/share/locale/pt_BR/LC_MESSAGES/warsaw.mo
rm -f /usr/share/man/man1/warsaw.1.gz

